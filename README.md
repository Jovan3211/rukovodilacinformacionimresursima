# RGIRNRGPKAGPINRGI
> Rukovodilac Generickim Informacionim Resursima Namenjeni za Rukovodjenje Generalnim Podacima Koji Asistiraju u Generickim Problemima Izazvane Nemanjem Rukovodilaca Generickih Informacija

Projekat iz SIMS-a i Baza Podataka.

### Pokretanje

Aplikacija se pokrece iz `App/main.py` bez parametara. Potrebno je instalirati `requirements.txt` u python environment pre pokretanja.

### PySide2 Enviroment error

Ako pokusate da koristite PySide2 na Windows masini, i naidjete na sledecu gresku:

`ImportError: DLL load failed: The specified module could not be found`

Jedno od resenja je koristiti instalaciju `Python 3.7.x` i instalirati PySide2 sledecom komandom: `pip install -i https://pypi.tuna.tsinghua.edu.cn/simple pyside2==5.14.2.1`