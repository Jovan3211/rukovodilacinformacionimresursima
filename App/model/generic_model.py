from PySide2 import QtGui,QtCore,QtWidgets
import json

class GenericModel(QtCore.QAbstractTableModel):
    def __init__(self, parent, file_handler, elements = []):
        super().__init__(parent)
        self.elements = elements
        self.file_handler = file_handler
        self.metadata = file_handler.metadata
        self.key_column = 0

        if self.metadata["type"] == "sekvencijalna":
            self.length = len(self.metadata["columns"])
            for i in range(self.length):
                if self.metadata["columns"][i] == self.metadata["key"]:
                    self.key_column = i
    
    def get_key(self):
        return self.file_handler.get_key()

    def get_column_names(self):
        return self.file_handler.get_column_names()

    def get_element(self, index):
        return self.elements[index.row()]

    def rowCount(self, index):
        return len(self.elements)
        
    def columnCount(self, index):
        return len(self.metadata['columns'])

    def data(self, index, role = QtCore.Qt.DisplayRole):
        element = self.get_element(index)
        if role == QtCore.Qt.DisplayRole:
            return element[self.metadata['columns'][index.column()]]
        if role == QtCore.Qt.BackgroundRole and index.column() == self.key_column and self.metadata["type"] == "sekvencijalna":
            return QtGui.QBrush(QtGui.QColor(155, 175, 250))

        return None

    def headerData(self, section, orientation, role = QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.metadata['columns'][section]

        return None

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        element = self.get_element(index)
        if value == " ":
            return False

        if role == QtCore.Qt.EditRole:
            element[self.metadata['columns'][index.column()]] = value
            try:
                self.file_handler.update(index.row(), element)
                return True
            except Exception as ex:
                msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Greska u izmeni", str(ex))
                msg.exec()
                #TODO: kada se promeni vrednost u readonly mode-u, vratiti na vrednost koja je pre stojala, pre izmene, u prikazu
                return False

        return False

    def flags(self, index):
        if index.column() == self.key_column and self.metadata["type"] == "sekvencijalna":
            return super().flags(index) | QtCore.Qt.NoItemFlags
        else:
            return super().flags(index) | QtCore.Qt.ItemIsEditable