from PySide2 import QtCore, QtGui
import operator

class DatabaseModel(QtCore.QAbstractTableModel):
    def __init__(self, parent = None, repository = None, elements = []):
        super().__init__(parent)
        self.elements = elements
        self.repository = repository
        self.key_column = repository.key
        self.key_column_counter = 0
        
        for i in self.repository.column_names:
            if i != self.repository.key:
                self.key_column_counter+=1
            else:
                break
    
    def get_key(self):
        return self.repository.get_key()

    def get_column_names(self):
        return self.repository.get_column_names()

    def get_element(self, index):
        return self.elements[index.row()]

    def rowCount(self, index):
        return len(self.elements)
        
    def columnCount(self, index):
        return len(self.repository.column_names)

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        element = self.get_element(index)
        if value == " ":
            return False

        if role == QtCore.Qt.EditRole:
            temp = list(element)
            temp[index.column()] = value
            element = tuple(temp)
            print("daa", element)
            self.elements[index.row()] = element
            self.repository.update(index, element)
            return True

        return False

    def data(self, index, role = QtCore.Qt.DisplayRole):
        element = self.get_element(index)
        if role == QtCore.Qt.DisplayRole:
            return str(element[index.column()])
        if role == QtCore.Qt.BackgroundRole and index.column() == self.key_column_counter:
            return QtGui.QBrush(QtGui.QColor(155, 175, 250))
        return None

    def headerData(self, section, orientation, role = QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.repository.column_names[section]

        return None

    def flags(self, index):
        if index.column() == self.key_column_counter:
            return super().flags(index) | QtCore.Qt.ItemIsSelectable
        else:
            return super().flags(index) | QtCore.Qt.ItemIsEditable