from util.serial_file_handler import SerialFileHandler
import json, os, shutil

class SequentialFileHandler(SerialFileHandler):
    def __init__(self, filepath, filepath_meta, readonly=False):
        # Pozivanje konstruktora iz serijskog file handlera
        super().__init__(filepath, filepath_meta, readonly)
        
        # Provera da li je datoteka odgovarajuceg tipa, ako nije vrsi ciscenje i daje exception
        if super().get_type() != "sekvencijalna":
            super().finish()
            raise Exception("Sequential file handler constructor: Pokusaj otvaranje datoteke ne sekvencijalnog tipa.")
        
        # Promenljiva koja oznacava kljucnu kolonu
        meta = super().get_meta()
        self.key_column = meta["key"]
        self.key_type = meta["key_type"]

    def __del__(self):
        # Pozivanje destruktora iz serijskog file handlera
        super().__del__()

    def get_key(self):
        """
        Vrati kljucnu kolonu datoteke.
        """
        return self.key_column

    def get_key_type(self):
        """
        Vrati data tip kljuca. Moze biti "str" ili "int".
        """
        return self.key_type

    def insert(self, obj):
        """
        Dodaje novi red u datoteku po redosledu kljuca. Kljuc je uvek prva kolona. Baca exception u slucaju da kljuc postoji.
        """
        if self.readonly:
            raise Exception("Datoteka je otvorena u 'readonly' modu.")

        # Resetuje cursor fajla na pocetak
        self.file_object_temp.seek(0)

        # Kreira privremeni fajl za pisanje
        write_temp_path = self.filePath_temp + ".tmp"
        write_temp = self.open_file(write_temp_path, "w")

        to_write = self.parse_dictionary(obj) + "\n"

        # Upisuje do linije koja treba da se menja
        exists = False
        written = False
        while True:
            line = self.file_object_temp.readline()

            # Ako se doslo do kraja fajla, upisi novu vrednost na kraj fajla
            if line == "\n" or line == "":
                if not written:
                    written = True
                    write_temp.write(to_write)
                break

            # Parsiranje linije
            parsed_line = self.to_dictionary(line)

            # U slucaju da kljuc postoji, prekida se insert
            if not written and parsed_line[self.key_column] == obj[self.key_column]:
                exists = True
                break

            # Ako je nadjen veci kljuc, upisi novu vrednost i nastavi pisanje do kraja fajla
            if not written and parsed_line[self.key_column] > obj[self.key_column]:
                written = True
                write_temp.write(to_write)

            # Upise liniju u novi temp fajlj
            write_temp.write(line)

        # Ako je nadjen postojeci key, baci exception i ocisti novi temp fajl
        if exists:
            self.close_file(write_temp)
            os.remove(write_temp_path)
            raise Exception("Kljuc vec postoji u datoteci.")

        # Zatvara privremene fajlove
        self.close_file(self.file_object_temp)
        self.close_file(write_temp)

        # Zamenjuje stari temp sa novim
        shutil.copyfile(write_temp_path, self.filePath_temp)
        os.remove(write_temp_path)

        # Otvara temp fajl
        self.file_object_temp = self.open_file(self.filePath_temp, "a+")

        print("Sequential file handler: Izvrseno insertovanje reda '{0}' u fajl '{1}'".format(str(obj), self.filePath_temp))
        self.changes = True

    def update(self, index, obj):
        """
        Zamenjuje red na datom indeksu sa datim objektom. Ako je drugaciji kljuc, bice smesten na odgovarajuce redosledno mesto.

        Parameters:
        indes:int - Indeks reda koji da se updateuje
        obj:dict - Dictionary sa novim vrednostima
        """
        if self.readonly:
            raise Exception("Datoteka je otvorena u 'readonly' modu.")

        # Dobijanje reda za update u slucaju brisanja, i ne uspevanja insertovanja
        old_row = self.get(index)

        # Ako je uspesno brisanje, pokusaj insertovanje novog objekta
        if super().delete(index):
            try:
                self.insert(obj)
                print("Sequential file handler: Izvrseno updateovanje reda '{0}' sa novim '{1}'".format(index, str(obj)))
                self.changes = True
            except Exception as ex:
                # Ako je neuspesno insertovanje (ako kljuc vec postoji) onda vrati stari obrisan red
                self.insert(old_row)
                print("Sequential file handler: Neuspesno dodavanje '{0}', novi kljuc vec postoji.".format(str(obj)))
                raise ex
        else:
            print("Sequential file handler: Nije nadjen index po kom da se updateuje, '{0}'".format(index))
