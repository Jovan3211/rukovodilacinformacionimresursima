# Putanje za fajlove datoteka
class Paths():
    path_data = "/data/"
    path_studenti = "data/studenti.data"
    path_predmeti = "data/predmeti.data"
    path_profesori = "data/profesori.data"
    path_biblioteke = "data/biblioteka.data"
    path_clanovi = "data/clanovi.data"
    path_knjige = "data/knjige.data"
    path_meta_studenti = "data/student_metadata.json"
    path_meta_predmeti = "data/predmet_metadata.json"
    path_meta_profesori = "data/profesor_metadata.json"
    path_meta_biblioteke = "data/biblioteka_metadata.json"
    path_meta_clanovi = "data/clanovi_metadata.json"
    path_meta_knjige = "data/knjige_metadata.json"

    def getMetaPath(data_path):
        if (Paths.path_studenti in data_path):
            return Paths.path_meta_studenti
        elif (Paths.path_predmeti in data_path):
            return Paths.path_meta_predmeti
        elif (Paths.path_profesori in data_path):
            return Paths.path_meta_profesori
        elif (Paths.path_biblioteke in data_path):
            return Paths.path_meta_biblioteke
        elif (Paths.path_clanovi in data_path):
            return Paths.path_meta_clanovi
        elif (Paths.path_knjige in data_path):
            return Paths.path_meta_knjige
        else:
            return None