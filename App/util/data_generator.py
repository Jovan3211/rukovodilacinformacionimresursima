import random
from util.serial_file_handler import SerialFileHandler

class DataGenerator():
    @staticmethod
    def generate_posetioci():
        names = ["Heather Palmer",
        "Judah Strong",
        "Aliesha Cook",
        "Sukhmani Morris",
        "Adaline Cox",
        "Izabela Mill",
        "Zack Farrell",
        "Amani Cervantes",
        "Asad Whitehouse",
        "Christine Lin",
        "Dilan Frey",
        "Ahmed Ellwood",
        "Esme Watson",
        "Carmel Richmond",
        "Scott Patel",
        "Tomasz Petersen",
        "Umar Baker",
        "Reef Best",
        "Nathaniel Firth",
        "Billy-Joe Lang",
        "Daanyaal Fleming",
        "Emyr Kearns",
        "Ace Chambers",
        "Beth Bryant",
        "Ned Moran",
        "Zayden Brewer",
        "Alexander Lam",
        "Nayla Blaese",
        "Skyla Case",
        "Romario Huber",
        "Millie Monaghan",
        "Alysha Rossi",
        "Umaiza Ewing",
        "Alison Medina",
        "Izabelle Waters",
        "Bronwen Dawe",
        "Annabel Clark",
        "Theo Mann",
        "Tyra Dominguez",
        "Marley Weaver",
        "Ayana Vincent",
        "Taryn Nguyen",
        "Mai Wright",
        "Agnes Alvarez",
        "Alexie Ahmad",
        "Lillie Couch",
        "Aron Torres",
        "Nichola Lawson",
        "Calvin Marriott",
        "Ihsan Cash",
        "James Rollins",
        "Monika House",
        "Evelyn Garrett",
        "Matas Clifford",
        "Bushra Clarke",
        "Mica Love",
        "Alexia Ellison",
        "Michele Riddle",
        "Cormac Austin",
        "Mariam Gibbs",
        "William Miranda",
        "Eloise Mccarty",
        "Catrina Duke",
        "Oscar Ramsay",
        "Jac Lake",
        "Timothy Payne",
        "Hayden Pearce",
        "Kenan Jacobson",
        "Pollyanna Leach",
        "Ilayda Britton",
        "Elly Dowling",
        "Emmy Munoz",
        "Tim Cornish",
        "Kailan Russell",
        "Ayush Chan",
        "Alberto Joyce",
        "Rocco Everett",
        "Dorothy Howell",
        "Selina Mcdougall",
        "Caitlin Hanna",
        "Larry Wiggins",
        "Macie Conner",
        "Mabel Walton",
        "Chance Keith",
        "Luke Salter",
        "Cathal Hoover",
        "Ayub Espinosa",
        "Marco Stark",
        "Serena Bradshaw",
        "Firat Perry",
        "Andrea Partridge",
        "Valentina Kelley",
        "Sheila James",
        "Lily-Anne Mccabe",
        "Jason East",
        "Angelina Grimes",
        "Amelia Lowry",
        "Coral Franklin",
        "Connor Barnett",
        "Haniya Houston",
        ]

        handler = SerialFileHandler("data/posetioci.data", "data/posetioci_metadata.json")

        print("Generisu se podaci...")

        counter = 0
        while counter < 1000000:
            for i in range(100):
                fullname = names[i]
                name, surname = fullname.split()
                pol = "M"
                if counter % 2:
                    pol = "Z"
                godina_rodjenja = str(random.randrange(1900, 2020, 1))
                broj_telefona = ("063" + str(counter) + "0000000")[:10]
                handler.insert({"ime":name, "prezime":surname, "pol":pol, "godina_rodjenja":godina_rodjenja, "broj_telefona":broj_telefona})
                counter += 1
                print(counter)

        handler.save()
        handler.finish()
        print("Zavrsena generacija")