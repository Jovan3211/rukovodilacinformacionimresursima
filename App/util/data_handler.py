from abc import ABC, abstractmethod
import os, json

class DataHandler():
    def __init__(self):
        super().__init__()

    @abstractmethod
    def get(self, index):
        pass

    #TODO: POTREBNO OVO PROMENITI DA NE POSTOJI, zameniti sa get_chunk(), koji se ucitava sa skrolovanjem tabele
    @abstractmethod
    def get_all(self):
        pass

    @abstractmethod
    def insert(self, obj):
        pass

    @abstractmethod
    def delete(self, index):
        pass

    @abstractmethod
    def update(self, index, obj):
        pass

    @abstractmethod
    def save(self):
        pass

    @abstractmethod
    def search(self):
        pass

    @abstractmethod
    def finish(self):
        pass

    @abstractmethod
    def get_column_names(self):
        pass

    @abstractmethod
    def get_type(self):
        pass