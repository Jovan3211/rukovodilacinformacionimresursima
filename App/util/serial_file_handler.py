from util.data_handler import DataHandler
import json, os, shutil

class SerialFileHandler(DataHandler):
    def __init__(self, filepath, filepath_meta, readonly=False):
        # readonly - gasi sve funkcionalnosti sem citanja iz datoteke, i ne kreira temp fajlove
        # Provera postojanja fajla i metapodataka
        if os.path.isfile(filepath) == False:
            print("Serial file handler constructor: Greska, pokusaj kreiranja SerialFileHandler-a, fajl ne postoji: '{0}'".format(filepath))
            raise Exception("Pokusaj otvaranja nepostojace datoteke.")
        if os.path.isfile(filepath_meta) == False:
            print("Serial file handler constructor: Greska, pokusaj kreiranja SerialFileHandler-a, fajl metapodataka ne postoji: '{0}'".format(filepath_meta))
            raise Exception("Pokusaj otvaranja datoteke sa nepostojacim metapodacima.")

        # Inicijalizacija klasnih promenljivih
        self.filePath = filepath
        self.filePath_temp = filepath + ".tmp"
        self.filePath_meta = filepath_meta
        self.metadata = {}
        self.readonly = readonly
        self.changes = False

        # Ucivatanje metapodataka
        self.load_meta()

        # Otvaranja fajlova za rad
        # Provera da li je datoteka slobodna za otvaranje
        try:
            self.file_object = self.open_file(self.filePath, "r")
            if readonly:
                print("Serial file handler constructor: Datoteka otvorena u 'readonly' modu '{0}'".format(self.filePath))
                return
        except IOError as ex:
            print("Serial file handler constructor: Neuspesno otvaranja datoteke '{0}'".format(self.filePath))
            raise ex

        # Kreiranje temp fajla
        shutil.copyfile(self.filePath, self.filePath_temp)
        self.file_object_temp = self.open_file(self.filePath_temp, "a+")

    def __del__(self):
        # U slucaju da fajlovi nisu otvoreni, nastavlja brisanje objekta bez ciscenja
        # U slucaju da su fajlovi otvoreni, zatvori ih i obrise temp fajl
        try:
            self.finish()
            print("Serial file handler destructor: Obrisan objekat sa ciscenjem.")
        except:
            print("Serial file handler destructor: Obrisan objekat bez ciscenja.")

    def finish(self):
        """
        Zatvara fajl i temp fajl, brise temp fajl.
        """
        self.close_file(self.file_object)

        if not self.readonly:
            self.close_file(self.file_object_temp)
            os.remove(self.filePath_temp)

            print("Serial file handler cleaner: Zatvorena datoteka, obrisan temporary fajl.")

    def load_meta(self):
        """
        Ucitava metapodatke u klasnu promenljivu.
        """
        with open(self.filePath_meta) as metadata:
            self.metadata = json.load(metadata)

    def get_meta(self):
        """
        Daje ucitane metapodatke datoteke.
        """
        return self.metadata

    def parse_dictionary(self, dictonary):
        """
        Uzima dictionary i pretvara ga u string sa zarezima bez naziva kolona.

        Paramaters:
        dictionary:dict - Dictionary koji da pretvori u string.

        Returns:
        result:str - String vrednosti iz dictionary-a razdvojen zarezima bez razmaka.

        Example:
        {"ime":"jovan", "prezime":"jovanovic", "godine":"63"} => "jovan,jovanovic,63"
        """

        parsed = ""
        for meta_column in self.metadata["columns"]:
            parsed += dictonary[meta_column] + ","
        parsed = parsed[:-1]
        return parsed

    def to_dictionary(self, line):
        """
        Uzima string i pretvara ga u dictionary u format naznacen metapodacima.

        Paramaters:
        line:string - Iscitana linija iz fajla.

        Returns:
        result:dict - Dictionary sa kolonama kao kljuc, i vrednostima iz unesenog stringa.

        Example:
        metadata[columns]: ["ime", "prezime", "godina_studija"]
        "jovan,jovanovic,63" => {"ime":"jovan", "prezime":"jovanovic", "godine":"63"}
        """
        line = line.rstrip()
        parsed = line.split(",")
        
        dictionary = {}
        counter = 0
        for meta_column in self.metadata["columns"]:
            dictionary[meta_column] = parsed[counter]
            counter += 1

        return dictionary

    def open_file(self, file_path, mode):
        """
        Pokusa da otvori fajl iz file_path-a sa prosledjenim modom.
        """
        try:
            print("Serial file handler: Otvaranje fajla '{0}'; mod '{1}'".format(file_path, mode))
            return open(file_path, mode)
        except IOError as ex:
            print("Serial file handler: Greska u otvaranju fajla '{0}'; mod: '{1}'; ex: {2}".format(file_path, mode, ex))
            raise ex

    def close_file(self, file):
        """
        Zatvara prosledjen fajl.
        """
        print("Serial file handler: Zatvoren fajl '{0}'".format(file.name))
        file.close()

    def save(self):
        """
        Sacuva podatke iz temp fajla u glavni fajl. Kopira temp preko originalno fajla.
        """
        if self.readonly:
            raise Exception("Datoteka je otvorena u 'readonly' modu.")

        # Zatvaranje fajlova
        self.close_file(self.file_object)
        self.close_file(self.file_object_temp)

        # Kopira izmenjen temp fajl preko glavnog
        shutil.copyfile(self.filePath_temp, self.filePath)
        print("Serial file handler: Sacuvana datoteka '{0}'".format(self.filePath))

        # Otvaranje fajlova za rad
        self.file_object = self.open_file(self.filePath, "r")
        self.file_object_temp = self.open_file(self.filePath_temp, "a+")

        self.changes = False

    def get_column_names(self):
        """
        Vraca sva imena kolona u listi.
        """
        return self.metadata["columns"]

    def get_type(self):
        """
        Vraca tip datoteke
        """
        return self.metadata["type"]

    def insert(self, obj):
        """
        Dodaje novi red u datoteku
        """
        if self.readonly:
            raise Exception("Datoteka je otvorena u 'readonly' modu.")

        to_write = self.parse_dictionary(obj)
        self.file_object_temp.write(to_write + "\n")
        print("Serial file handler: Unesen novi red u '{0}'".format(self.filePath_temp))
        self.changes = True

    def get(self, index):
        """
        Dobija parsiran dictionary po indeksu iz temp datoteke.
        Ako je u readonly modu cita iz glavnog tekstualnog fajla.

        Paramaters:
        index:int - Indeks reda datoteke

        Returns: 
        Parsiran dictionary, ili None u slucaju da red ne postoji
        """
        file_to_read = None
        if self.readonly:
            file_to_read = self.file_object
        else:
            file_to_read = self.file_object_temp
        
        # Resetuje cursor fajla na pocetak
        file_to_read.seek(0)

        counter = 0
        while True:
            line = file_to_read.readline()
            
            if line == "\n" or line == "" or counter > index:
                print("Serial file handler get: Nije nadjen trazeni indeks '{0}' u datoteci '{1}'".format(index, file_to_read))
                return None

            if counter == index:
                parsed = self.to_dictionary(line)
                print("Serial file handler get: Dobiven indeks '{0}' ({1}) u datoteci '{2}'".format(index, parsed, file_to_read))
                return parsed

            counter += 1
        
    def get_all(self):
        #TODO: privremeno resenje, pogledati data_handler.py za dalje instrukcije
        data = []
        file_to_read = None
        if self.readonly:
            file_to_read = self.file_object
        else:
            file_to_read = self.file_object_temp

        file_to_read.seek(0)

        print("Serial file handler: Iscitavanje podataka iz '{0}'".format(file_to_read.name))
        while True:
            line = file_to_read.readline()
            
            if line == "\n" or line == "":
                break

            parsed = self.to_dictionary(line)
            data.append(parsed)

        return data

    def search(self, arguments):
        """
        Pretrazi datoteku po unetim argumentima

        Paramaters:
        arguments:dict - Dictionary sa unetim filterima za pretragu, i odgovarajucim nazivima kolona za kljuc

        Returns:
        Dictionary list - Vraca listu dictionary-a nadjenih vrednosti. Vrati praznu listu ako nije nista nadjeno.
        """
        file_to_read = None
        if self.readonly:
            file_to_read = self.file_object
        else:
            file_to_read = self.file_object_temp

        # Resetuje cursor fajla na pocetak
        file_to_read.seek(0)

        # Lista za nadjene elemente
        findings = []

        # Ako su argumenti prazni vrati get_all()
        content = False
        for value in arguments.values():
            if value != "":
                content = True
                break
        if not content:
            return self.get_all()

        while True:
            line = file_to_read.readline()
            
            # Na kraju fajla vrati nadjene elemente
            if line == "\n" or line == "":
                print("Serial file handler: Izvrsena pretraga, nadjeno '{0}' rezultat za argumente '{1}' u '{2}'".format(len(findings), arguments, file_to_read.name))
                print(findings)
                return findings

            parsed_line = self.to_dictionary(line)

            # Provera da li se sadrzi substring u datoteci za tu kolonu
            for column in self.metadata["columns"]:
                if arguments[column].lower() == parsed_line[column].lower() and parsed_line[column] != "": 
                    findings.append(parsed_line)
                    break

    def delete(self, index):
        """
        Obrise jednu liniju na indeksu u temp fajlu.

        Paramaters:
        index:int - Indeks koji red koji da se obrise

        Returns:
        boolean - 'True' ako je izvrseno brisanje, 'False' ako nije nadjen indeks
        """
        return self.delete_many([index])

    def delete_many(self, indexes):
        """
        Obrise vise linija u temp fajlu.

        Paramaters:
        indexes:list - Lista integera sa indeksima koji da se obrisu

        Returns:
        boolean - 'True' ako je izvrseno brisanje, 'False' ako nije nadjen indeks
        """
        if self.readonly:
            raise Exception("Datoteka je otvorena u 'readonly' modu.")

        # Resetuje cursor fajla na pocetak
        self.file_object_temp.seek(0)

        # Kreira privremeni fajl za pisanje
        write_temp_path = self.filePath_temp + ".tmp"
        write_temp = self.open_file(write_temp_path, "w")

        # Uzima najveci indeks u indeksima
        max_index = max(indexes)

        print("Serial file handler: Vrsenje brisanja nad fajlom '{0}'".format(self.filePath_temp))

        edited = False

        # Upisuje sve linije, koje nisu u indeksima za brisanje, u drugi temp fajl
        counter = 0
        while True:
            line = self.file_object_temp.readline()

            if line == "\n" or line == "":
                break

            if counter not in indexes:
                write_temp.write(line)
            else:
                edited = True
            counter += 1

        # Ako nije nadjen index za brisanje, i ako nemaju promene koje da se sacuvaju, obrisi drugi temp fajl
        if not edited:
            self.close_file(write_temp)
            os.remove(write_temp_path)
            print("Serial file handler: Nije nadjen index po kom da obrise")
            return False

        # Zatvara privremene fajlove
        self.close_file(self.file_object_temp)
        self.close_file(write_temp)

        # Zamenjuje stari temp sa novim
        shutil.copyfile(write_temp_path, self.filePath_temp)
        os.remove(write_temp_path)

        # Otvara temp fajl
        self.file_object_temp = self.open_file(self.filePath_temp, "a+")

        print("Serial file handler: Izvrseno brisanje")
        self.changes = True
        return True

    def update(self, index, obj):
        """
        Zamenjuje red na datom indeksu sa datim objektom

        Parameters:
        indes:int - Indeks reda koji da se updateuje
        obj:dict - Dictionary sa novim vrednostima
        """
        if self.readonly:
            raise Exception("Datoteka je otvorena u 'readonly' modu.")

        # Resetuje cursor fajla na pocetak
        self.file_object_temp.seek(0)

        # Kreira privremeni fajl za pisanje
        write_temp_path = self.filePath_temp + ".tmp"
        write_temp = self.open_file(write_temp_path, "w")

        not_found = False

        # Upisuje do linije koja treba da se menja
        counter = 0
        while True:
            line = self.file_object_temp.readline()
            if line == "\n" or line == "":
                not_found = True
                break

            if counter == index:
                break

            write_temp.write(line)
            counter += 1

        # Ako index nije nadjen, zatvara drugi temp i brise ga
        if not_found:
            self.close_file(write_temp)
            os.remove(write_temp_path)
            print("Serial file handler: Nije nadjen index po kom da se updateuje")
            return

        # Upisi updateovanu vrednost
        to_write = self.parse_dictionary(obj)
        write_temp.write(to_write + "\n")

        # Zavrsi pisanje do kraja fajla
        while True:
            line = self.file_object_temp.readline()

            if line == "\n" or line == "":
                break

            write_temp.write(line)

        # Zatvara privremene fajlove
        self.close_file(self.file_object_temp)
        self.close_file(write_temp)

        # Zamenjuje stari temp sa novim
        shutil.copyfile(write_temp_path, self.filePath_temp)
        os.remove(write_temp_path)

        # Otvara temp fajl
        self.file_object_temp = self.open_file(self.filePath_temp, "a+")

        print("Serial file handler: Izvrseno updateovanje reda '{0}' u fajlu '{1}' sa vrednosti '{2}'".format(index, self.filePath_temp, str(obj)))
        self.changes = True
