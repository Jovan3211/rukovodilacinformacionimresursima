from util.data_handler import DataHandler
import json
import pickle
import mysql.connector

def get_connection(new_user, new_password, new_host, new_database):
    return mysql.connector.connect(user= new_user, password=new_password, host=new_host, database=new_database)

class DatabaseHandler(DataHandler):
    def __init__(self, table, key, metapath):
        super().__init__()
        try:
            self.table = table
            print(self.table)
            self.key = key
            print(self.key)
            self.metapath = metapath

            self.readonly = False

            with open(self.metapath, "rb") as metadata:
                self.baza = db_metadata = json.load(metadata)

            self.cnx = get_connection(db_metadata["user"], db_metadata["password"], db_metadata["host"], db_metadata["database"])
            self.cursor = self.cnx.cursor()
            self.data = self.load_data()
            self.column_names = self.cursor.column_names
            print(self.column_names)

        except mysql.connector.Error as err: 
            print("Failed: {}".format(err))
            self.cnx.close()
    
    def get_type(self):
        return "baza"

    def get_key(self):
        return self.key

    def get_column_names(self):
        return self.column_names

    def finish(self):
        self.close()

    def __exit__(self, type__, value, traceback):
        self.close()

    def __enter__(self):
        return self

    def close(self):
        if self.cnx:
            self.cnx.commit()
            self.cnx.close()
        print("DatabaseHandler: Zatvorena konekcija sa bazom")

    def complete(self):
        self.__complete = True

    def load_data(self):
        return self.get_all()
    
    def search(self, arguments):
        obj = tuple(arguments.values())

        # Ako su argumenti prazni vrati get_all()
        content = False
        for value in arguments.values():
            if value != "":
                content = True
                break
        if not content:
            return self.get_all()

        # Dobij rezultate
        results = self.get(obj)

        return results

    def get(self, elements):
        try:
            search = self.baza[self.table]["search"]
            self.cursor.callproc(search, elements)

            for element in self.cursor.stored_results():
                num_fields = len(element.description)
                self.column_names = [i[0] for i in element.description]
                result = element.fetchall()
            print("ovo je result", result)
            return result
            #self.cursor.execute("SELECT * FROM "+ self.table +" WHERE " + self.key + " = " + id)
            #result = self.cursor.fetchall() 
            self.cnx.commit()
            #return result
        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
    
    def get_all(self):
        try:
            read = self.baza[self.table]["read"]
            self.cursor.callproc(read)
            print("vovovo", self.table)

            for element in self.cursor.stored_results():
                num_fields = len(element.description)
                self.column_names = [i[0] for i in element.description]
                result = element.fetchall()
            return result
        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
    
    def update(self, index, obj):
        try:
            print("ovo je index", index)
            update = self.baza[self.table]["update"]
            #args = (obj[0:])
            self.cursor.callproc(update, obj)
            self.cnx.commit()

        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
            self.cnx.rollback()
            self.cnx.commit()
            
    def delete(self, key_value):
        try:
            delete = self.baza[self.table]["delete"]
            args = (str(key_value), )
            self.cursor.callproc(delete, args)

            self.cnx.commit()
        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
            self.cnx.rollback()
            self.cnx.commit()
            raise err
    
    def insert(self, dictionary):
        # Konvertovanje dictionary-a u tuple
        obj = tuple(dictionary.values())

        try:
            print("ovoje", obj)
            create = self.baza[self.table]["create"]
            args = (obj[0:])
            print("ovo su args", args)
            self.cursor.callproc(create, args)
            self.cnx.commit()
        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
            self.cnx.rollback()
            self.cnx.commit()
            raise err
    
    def insert_many(self, dictionary):
        obj = tuple(dictionary.values())

        for i in obj:
            self.insert(i)
    
    def save(self):
        print("DataBaseHandler: Baza direktno unosi podatke u sebe.")