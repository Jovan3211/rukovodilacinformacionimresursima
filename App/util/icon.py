from PySide2 import QtGui

#Klasa konstanti za putanje ikona
class Icon():
    PATH = "util/icons/"
    APP_LOGO = PATH + "icons8-edit-file-64.png"
    SAVE = PATH + "icons8-save-64.png"
    OPEN = PATH + "folder-horizontal-open.png"
    PACKAGE_NEW = PATH + "paper-bag--plus.png"
    TABLE_NEW = PATH + "table--plus.png"
    ERROR = PATH + "exclamation-red.png"
    DELETE = PATH + "delete.png"
    INSERT_ROW = PATH + "icons8-plus-64.png"
    DELETE_ROW = PATH + "icons8-minus-64.png"
    KEY = PATH + "key.png"
    WARNING = PATH + "icons8-warning-48.png"
    SERIAL_DOC = PATH + "icons8-document-64.png"
    SEQUENTIAL_DOC = PATH + "icons8-document-key-64.png"
    SEARCH = PATH + "icons8-search-64.png"
    DATABASE_DOC = PATH + "icons8-database-16.png"
    RESET = PATH + "icons8-reset-48.png"

    # Vraca QIcon koji sadrzi ikonu za odgovarajuci filetype
    @staticmethod
    def get_icon_for_filetype(file_handler):
        icon = QtGui.QIcon(Icon.APP_LOGO)
        if file_handler.get_type() == "sekvencijalna":
            icon = QtGui.QIcon(Icon.SEQUENTIAL_DOC)
        elif file_handler.get_type() == "serijska":
            icon = QtGui.QIcon(Icon.SERIAL_DOC)
        elif file_handler.get_type() == "baza":
            icon = QtGui.QIcon(Icon.DATABASE_DOC)

        return icon