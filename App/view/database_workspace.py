from PySide2 import QtWidgets, QtGui, QtCore
from model.db_model import DatabaseModel
from model import *
from util.db_handler import DatabaseHandler
from view.search_widget import SearchWidget
from view.database_tool_bar import DatabaseToolBar

class DatabaseWorkspace(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.tab_widget = None
        self.sql_handler = None
        self.new_element = {}
        self.search_element = ()
        self.searching = ()
        self.new_element_values = []
        self.inserting_column = None
        self.insert_dict = {}
        self.create_tab_widget()
        self.selected = 0

        self.main_table = QtWidgets.QTableView(self.tab_widget)
        self.main_table.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.main_table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.main_table.setModel(None)
        self.main_table.clicked.connect(self.row_selected)

        toolbar = DatabaseToolBar(self)
        search_container = SearchWidget(self.sql_handler, toolbar, self)

        self.main_layout.addWidget(search_container)
        self.main_layout.addWidget(self.main_table)
        self.main_layout.addWidget(self.tab_widget)
        self.setLayout(self.main_layout)

    def row_selected(self, index):
        self.selected = index.row()
        model = self.main_table.model()
        selected_data = model.get_element(index)
        print(selected_data)


    def create_tab_widget(self):
        self.tab_widget = QtWidgets.QTabWidget(self)
        self.tab_widget.setTabsClosable(True)
        self.tab_widget.tabCloseRequested.connect(self.delete_tab)

    def delete_tab(self, index):
        self.tab_widget.removeTab(index)

    def refresh_model(self, collection):
        print("Search called")
        print(collection)

    def refresh_table(self, sql_handler): 
        sql_model = DatabaseModel(self, sql_handler, sql_handler.get_all())
        sql_model.elements = sql_handler.get_all()
        print(sql_model.elements)
        self.main_table_elements = sql_model.elements
        self.sql_handler = sql_handler
        self.main_table.setModel(sql_model)
    
    def search_form(self):
        self.dialog = QtWidgets.QDialog()
        self.button = QtWidgets.QPushButton("Pretrazi")
        layout = QtWidgets.QVBoxLayout()
        self.search_element = ()

        for column in self.sql_handler.column_names:
            horizontal = QtWidgets.QHBoxLayout()
            label = QtWidgets.QLabel(column)
            self.value = QtWidgets.QLineEdit()
            self.insert_dict[column] = self.value
            horizontal.addWidget(label)
            horizontal.addStretch(300)
            horizontal.addWidget(self.value)

            layout.addLayout(horizontal)
            self.searching = column
        
        layout.addWidget(self.button)
        self.dialog.setWindowTitle("Pretraga")
        self.dialog.setWhatsThis("Unesite vrednosti kolona za pretragu")
        self.dialog.setLayout(layout)

        self.button.clicked.connect(self.search)
        self.dialog.show()
        
    def search(self):
        # try:
        model = self.main_table.model()
        temp_list = []
        for column in self.sql_handler.column_names:
            temp_list.append(self.insert_dict[column].text())

        temp = tuple(temp_list)
        print("pavle ovo je temp", temp)
        #searched = self.sql_handler.get_one(temp)
        self.main_table.setModel(model)
        self.dialog.accept()
        #print("ovde")
        #print("ovo su sercd", searched)
        sql_model = DatabaseModel(self, self.sql_handler, self.sql_handler.get_one(temp))
        sql_model.elements = self.sql_handler.get_one(temp)
        if sql_model.elements != []:

            self.main_table_elements = sql_model.elements
            self.main_table.setModel(sql_model)
        else:
            self.message = QtWidgets.QMessageBox()
            self.message.setWindowTitle("Neuspesna pretraga!")
            self.message.setText("Podatke koje ste uneli nisu pronadjeni u bazi!\nDa li zelite da unesete nove podatke za pretragu?")
            self.button = QtWidgets.QPushButton("Unesi")
            self.message.addButton(self.button, QtWidgets.QMessageBox.ActionRole)
            self.message.setStandardButtons(QtWidgets.QMessageBox.Cancel)
            self.button.clicked.connect(self.search_form)
            self.message.exec()

            sql_model = DatabaseModel(self, self.sql_handler, self.sql_handler.get_all())
            sql_model.elements = self.sql_handler.get_all()
        # except Exception as error:
        #     self.dialog.accept()
        #     self.message = QtWidgets.QMessageBox()
        #     self.message.setWindowTitle("Pretraga.")
        #     self.message.setText("Podatke koje pretrazujete nisu pronadjeni u bazi.")
        #     self.message.setStandardButtons(QtWidgets.QMessageBox.Cancel)
        #     self.message.exec()
    # def save(self):
    #     self.sql_handler.save()

    def delete_form(self):
        self.message = QtWidgets.QMessageBox()
        self.message.setWindowTitle("Brisanje reda")
        self.message.setText("Da li ste sigurni da zelite da obrisete red?")
        self.button = QtWidgets.QPushButton("Obrisi")
        self.message.addButton(self.button, QtWidgets.QMessageBox.ActionRole)
        self.message.setStandardButtons(QtWidgets.QMessageBox.Cancel)
        

        self.button.clicked.connect(self.delete)
        self.message.exec()
        
    def delete(self):
        try:
            indexes = self.main_table.selectedIndexes()
            if len(indexes) == 0:
                return False

            if len(indexes) > 0:
                row_num = indexes[0].row()
                element = self.main_table.model().elements[row_num]
                self.sql_handler.delete_one(element[0])
                self.refresh_table(self.sql_handler)
        except Exception as error:
            self.message = QtWidgets.QMessageBox()
            self.message.setWindowTitle("Brisanje reda.")
            self.message.setText("Brisanje neuspesno!\nNe mozete obrisati red ciji se kljuc koristi u drugoj tabeli.")
            self.message.setStandardButtons(QtWidgets.QMessageBox.Cancel)
            #self.message.exec()


    def insert_form(self):
        self.dialog = QtWidgets.QDialog()
        self.button = QtWidgets.QPushButton("Dodaj")
        layout = QtWidgets.QVBoxLayout()
        self.new_element = {}

        for column in self.sql_handler.column_names:
            horizontal = QtWidgets.QHBoxLayout()
            label = QtWidgets.QLabel(column)
            self.value = QtWidgets.QLineEdit()
            self.insert_dict[column] = self.value
            horizontal.addWidget(label)
            horizontal.addStretch(300)
            horizontal.addWidget(self.value)
            
            layout.addLayout(horizontal)
            self.inserting_column = column
        
        layout.addWidget(self.button)
        self.dialog.setWindowTitle("Unos novog reda")
        self.dialog.setWhatsThis("Unesite vrednosti kolona")
        self.dialog.setLayout(layout)

        self.button.clicked.connect(self.insert)
        self.dialog.show()

    def insert(self):
        try:
            model = self.main_table.model()
            temp_list = []
            for column in self.sql_handler.column_names:
                temp_list.append(self.insert_dict[column].text())

            temp = tuple(temp_list)
            model.elements.append(temp)
            inserted = self.sql_handler.insert(temp)
            self.main_table.setModel(model)
            self.dialog.accept()
            sql_model = DatabaseModel(self, self.sql_handler, self.sql_handler.get_all())
            sql_model.elements = self.sql_handler.get_all()
            self.main_table_elements = sql_model.elements
            self.main_table.setModel(sql_model)
            #upit za ponovni insert
            self.message = QtWidgets.QMessageBox()
            self.message.setWindowTitle("Dodavanje reda")
            self.message.setText("Red je uspesno dodat!\nDa li zelite da nastavite sa dodavanjem?")
            self.button = QtWidgets.QPushButton("Dodaj")
            self.message.addButton(self.button, QtWidgets.QMessageBox.ActionRole)
            self.message.setStandardButtons(QtWidgets.QMessageBox.Cancel)
            

            self.button.clicked.connect(self.insert_form)
            self.message.exec()

        except Exception as error:
            self.dialog.accept()
            self.message = QtWidgets.QMessageBox()
            self.message.setWindowTitle("Dodavanje reda.")
            self.message.setText("Dodavanje neuspesno! Proverite unesene podatke!")
            self.message.setStandardButtons(QtWidgets.QMessageBox.Cancel)
            self.message.exec()