from PySide2 import QtWidgets, QtGui, QtCore
from util.icon import Icon

#Toolbar koji se nalazi na centralnom widgetu
class ToolBar(QtWidgets.QToolBar):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent

        self.addAction(QtGui.QIcon(Icon.TABLE_NEW), "Nova datoteka", self.main_window.create_new_table)
        self.addAction(QtGui.QIcon(Icon.OPEN), "Otvori direktorium...", self.main_window.structure_dock.openFolder)