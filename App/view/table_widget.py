from PySide2 import QtWidgets, QtGui, QtCore
from model.generic_model import GenericModel
from model.db_model import DatabaseModel
from view.table_tool_bar import TableToolBar
from view.insert_dialog import InsertDialog
from view.search_widget import SearchWidget
from os import path

class TableWidget(QtWidgets.QWidget):
    def __init__(self, file_handler, parent=None):
        super().__init__(parent)

        # Kreiranje glavnog layout-a table_widget-a
        layout = QtWidgets.QVBoxLayout()

        # Definisanje file_handler-a
        self.file_handler = file_handler

        # Definisanje toolbar-a
        toolbar = TableToolBar(self, file_handler.readonly, file_handler.get_type() == "baza")

        # Podesavanje prikazne tabele
        self.table_view = QtWidgets.QTableView()
        self.table_view.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.table_view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        # Podesavanje table_modela i dodavanje na table_view
        generic_model = None
        if file_handler.get_type() == "baza":
            generic_model = DatabaseModel(self, file_handler, file_handler.get_all())
        else:
            generic_model = GenericModel(self, file_handler)
            generic_model.elements = file_handler.get_all()
        self.table_view.setModel(generic_model)

        # Toolbar container
        self.toolbar_container = SearchWidget(file_handler, toolbar, self)

        # Dodavanje elemenata na layout
        layout.addWidget(self.toolbar_container)
        layout.addWidget(self.table_view)
        self.setLayout(layout)

    # Vrati file handler objekat koji koristi tabela
    def getFileHandler(self):
        return self.file_handler

    # Osvezava model tabele (redraw view)
    def refresh_model(self, elements=None):
        generic_model = None
        if self.file_handler.get_type() == "baza":
            if elements == None:
                generic_model = DatabaseModel(self, self.file_handler, self.file_handler.get_all())
            else:
                generic_model = DatabaseModel(self, self.file_handler, elements)
        else:
            generic_model = GenericModel(self, self.file_handler)
            if (elements == None):
                generic_model.elements = self.file_handler.get_all()
            else:
                generic_model.elements = elements

        self.table_view.setModel(generic_model)

    # Poziva destruktora file handlera
    def close(self):
        self.file_handler.finish()

    def save(self):
        self.file_handler.save()
        msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Sacuvano: " + path.basename(self.file_handler.filePath), "Datoteka uspesno sacuvana")
        msg.exec()

    def search(self):
        self.toolbar_container.search()

    def new_row(self):
        if self.table_view.model() == None:
            return
        
        result = True
        while result:
            result = InsertDialog.create_new_row(self, self.table_view.model(), self.file_handler)
            self.refresh_model()

    def delete_row(self):
        #TODO: napraviti da se moze obrisati svi selektovani redovi
        index = self.table_view.selectedIndexes()
        if len(index) == 0:
            return False

        rows = index[0].row()
        row = self.table_view.model().elements[rows]

        #self.file_handler.delete_one(row[self.file_handler.metadata["key"]]) # kod za brisanje preko vrednosti kljuca
        try:
            if self.file_handler.get_type() == "baza":
                self.file_handler.delete(row[0])
            else:
                self.file_handler.delete(rows) # kod za brisanje preko selektovanog reda
        except Exception as ex:
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Greska u brisanju", str(ex))
            msg.exec()

        self.refresh_model()

    # Zakomentarisan pokusaj da popravim da moze da se obrise vise redova, iznad se nalazi stari kod koji funkcionise
    # za jedan selektovan red
    #
    # def delete_row(self):
    #     indexes = self.table_view.selectedIndexes()

    #     if len(indexes) == 0:
    #         return False

    #     rows = []
    #     for index in indexes:
    #         # Potrebna je ova provera zato sto se za jedan selektovan red racuna svaka kolona
    #         # koja je, takodje, selektovana. Sprecava kreaciju duplikata indeksa u rows.
    #         if index.row() not in rows:
    #             rows.append(index.row())

    #     for row_index in rows: 
    #         row = self.table_view.model().elements[row_index]
    #         self.file_handler.delete_one(row[self.file_handler.metadata["key"]])

    #     #TODO: buguje i ne radi kako treba

    #     self.refresh_model()