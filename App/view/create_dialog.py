from PySide2 import QtWidgets, QtGui, QtCore
from util.icon import Icon
import os

class CreateDialog(QtWidgets.QDialog):
    def __init__(self, parent):
        super(CreateDialog, self).__init__(parent, QtCore.Qt.Dialog)
        self.setupUi(self)

        self.fileHandler_types = ["serijska", "sekvencijalna"]
        self.fileHandler_types_descrpiptions = ["Serijska organizacija datoteke omogucava da se podaci upisuju redosledom kojim nastaju. Unutar serijske organizacije ne postoji struktura.", "Sekvencijalnom organizacijom su slogovi na mediju za datoteku fizicki smesteni jedan iza drugog u redosledu u kojem je datoteka kreirana i u istom redosledu se mogu obradjivati. Datoteka je sortirana po nekom kljucu a citanje se vrsi fizici u rastucem ili u opadajucem redosledu kljuca."]

        #self.label_filehandler_description.setText(self.fileHandler_types_descrpiptions[0])
        self.comboBox_filehandler_type.addItems(self.fileHandler_types)
        self.comboBox_filehandler_type.currentIndexChanged.connect(self.handle_combobox_change)

        self.button_browse.clicked.connect(self.select_file)
        self.button_create.clicked.connect(self.create_file)

    def setupUi(self, CreateDialog):
        CreateDialog.setObjectName("CreateDialog")
        CreateDialog.resize(400, 315)
        self.button_browse = QtWidgets.QPushButton(CreateDialog)
        self.button_browse.setGeometry(QtCore.QRect(310, 20, 75, 21))
        self.button_browse.setObjectName("button_browse")
        self.label_putanja = QtWidgets.QLabel(CreateDialog)
        self.label_putanja.setGeometry(QtCore.QRect(20, 20, 41, 16))
        self.label_putanja.setObjectName("label_putanja")
        self.lineEdit_path = QtWidgets.QLineEdit(CreateDialog)
        self.lineEdit_path.setGeometry(QtCore.QRect(70, 20, 231, 20))
        self.lineEdit_path.setObjectName("lineEdit_path")
        self.comboBox_filehandler_type = QtWidgets.QComboBox(CreateDialog)
        self.comboBox_filehandler_type.setGeometry(QtCore.QRect(160, 60, 141, 22))
        self.comboBox_filehandler_type.setObjectName("comboBox_filehandler_type")
        self.label_tip_rukovodioca = QtWidgets.QLabel(CreateDialog)
        self.label_tip_rukovodioca.setGeometry(QtCore.QRect(20, 60, 131, 21))
        self.label_tip_rukovodioca.setObjectName("label_tip_rukovodioca")
        self.label_warning_text = QtWidgets.QLabel(CreateDialog)
        self.label_warning_text.setGeometry(QtCore.QRect(80, 260, 301, 41))
        self.label_warning_text.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_warning_text.setWordWrap(True)
        self.label_warning_text.setObjectName("label_warning_text")
        self.label_warning_icon = QtWidgets.QLabel(CreateDialog)
        self.label_warning_icon.setGeometry(QtCore.QRect(20, 260, 41, 41))
        self.label_warning_icon.setText("")
        self.label_warning_icon.setPixmap(QtGui.QPixmap(Icon.WARNING))
        self.label_warning_icon.setScaledContents(True)
        self.label_warning_icon.setObjectName("label_warning_icon")
        self.button_create = QtWidgets.QPushButton(CreateDialog)
        self.button_create.setGeometry(QtCore.QRect(310, 230, 75, 23))
        self.button_create.setObjectName("button_create")
        self.label_kolone = QtWidgets.QLabel(CreateDialog)
        self.label_kolone.setGeometry(QtCore.QRect(20, 110, 41, 16))
        self.label_kolone.setObjectName("label_kolone")
        self.lineEdit_columns = QtWidgets.QLineEdit(CreateDialog)
        self.lineEdit_columns.setGeometry(QtCore.QRect(70, 110, 311, 20))
        self.lineEdit_columns.setObjectName("lineEdit_columns")
        self.label_kolone_opis = QtWidgets.QLabel(CreateDialog)
        self.label_kolone_opis.setGeometry(QtCore.QRect(70, 140, 311, 61))
        self.label_kolone_opis.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_kolone_opis.setObjectName("label_kolone_opis")
        self.retranslateUi(CreateDialog)
        QtCore.QMetaObject.connectSlotsByName(CreateDialog)

    def retranslateUi(self, CreateDialog):
        _translate = QtCore.QCoreApplication.translate
        CreateDialog.setWindowTitle(_translate("CreateDialog", "Nova datoteka"))
        self.button_browse.setText(_translate("CreateDialog", "Pretraga"))
        self.label_putanja.setText(_translate("CreateDialog", "Putanja:"))
        self.label_tip_rukovodioca.setText(_translate("CreateDialog", "Tip rukovodioca datoteke:"))
        self.label_warning_text.setText(_translate("CreateDialog", "Datoteka mora imati svoje json metapodatke pored svog fajla.\n""Ako se datoteka zove \'fajl.data\' mora takodje na istoj lokaciji postojati \'fajl_metadata.json\'."))
        self.button_create.setText(_translate("CreateDialog", "Kreiraj"))
        self.label_kolone.setText(_translate("CreateDialog", "Kolone:"))
        self.label_kolone_opis.setText(_translate("CreateDialog", "Potrebno je napisati kolone razdvojene zarezom. Primer:\n\"kolona1, kolona2, kolona3, kolona4, ...\"\n\nKljuc je uvek prva kolona."))
   
    def handle_combobox_change(self):
        self.label_filehandler_description.setText(self.fileHandler_types_descrpiptions[self.comboBox_filehandler_type.currentIndex()])

    # Otvara dialog gde korisnik bira lokaciju i filename nove datoteke
    def select_file(self):
        file_path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Nova datoteka", "datoteka", "Data files (*.data)")
        self.lineEdit_path.setText(file_path)
        return file_path

    # Pokusava da kreira novu datoteku sa unetim podacima
    def create_file(self):
        column_list = self.lineEdit_columns.text().replace(" ", "").split(",")
        file_path = self.lineEdit_path.text()
        json = '{\n    "columns": ' + str(column_list).replace("'", '"') +',\n    "key": "' + column_list[0] + '",    "key_type":"str",\n    "linked_files":[],\n    "type": "' + self.comboBox_filehandler_type.currentText() +'"\n}'
        json_nokey = '{\n    "columns": ' + str(column_list).replace("'", '"') +',\n    "linked_files":[],\n    "type": "' + self.comboBox_filehandler_type.currentText() +'"\n}'

        # TODO  Dodati lepse kreiranje kolona
        #       Provera da li nova datoteka uopste ima kolona

        if file_path == "":
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Nepravilno popunjeni podaci", "Putanja ne sme da ostane prazna.")
            msg.exec()
            return
            
        try:
            with open(file_path, "w") as data_file:
                data_file.write("")

            with open(file_path.replace(".data", "_metadata.json"), "w") as json_file:
                json_file.write(json)

            self.accept()
        except IOError as ex:
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska u kreiranju datoteke", "Desila se greska kod kreiranja datoteke:\n" + str(ex))
            msg.exec()
            print(ex)

    # Javna metoda za otvaranje dialoga i vracanje rezultata
    @staticmethod
    def create_new_table(parent):
        dialog = CreateDialog(parent)
        dialog_result = dialog.exec()
        return (dialog_result, dialog.lineEdit_path.text())
