from PySide2 import QtWidgets, QtGui, QtCore

from database.db_generic import DatabaseGeneric
import json

class DatabaseStructureDock(QtWidgets.QDockWidget):
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.main_window = parent
        self.metadata = None
        self.metapath = None
        ext = ["*.json"]


        #MODEL
        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath(QtCore.QDir.currentPath())
        self.model.setFilter(QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot | QtCore.QDir.AllEntries)
        self.model.setNameFilters(ext)
        self.model.setNameFilterDisables(False)

        #TREE
        self.tree = QtWidgets.QTreeView()
        self.tree.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tree.setModel(self.model)
        self.tree.setRootIndex(self.model.index(QtCore.QDir.currentPath() + "/database"))
        self.setWidget(self.tree)
        self.tree.clicked.connect(self.item_model)

    def item_model(self, index):
        self.metapath = self.model.filePath(index)
        print(self.metapath)
        self.model = QtGui.QStandardItemModel()
        self.model.setHorizontalHeaderLabels(['Database'])
        self.set_items(self.model)
        self.tree.setModel(self.model)
        self.tree.clicked.connect(self.item_selected)

    def set_items(self, tree):
        parent =  QtGui.QStandardItem('Database') 
        with open(self.metapath, "rb") as metadata:
            self.metadata = json.load(metadata)

        for table in self.metadata["tables"]:
            table_name = table["name"]
            row = QtGui.QStandardItem(table_name)
            parent.appendRow(row)

        button = QtGui.QStandardItem("Povratak na ostale baze")
        parent.appendRow(button)
        tree.appendRow(parent)

    def get_metadata(self):
        self.model = QtWidgets.QFileSystemModel()
        ext = ["*.json"]
        self.tree = QtWidgets.QTreeView()
        self.tree.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
    
        self.model.setRootPath(QtCore.QDir.currentPath())
        self.model.setFilter(QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot | QtCore.QDir.AllEntries)
        self.model.setNameFilters(ext)
        self.model.setNameFilterDisables(False)

        self.tree.setModel(self.model)
        self.tree.setRootIndex(self.model.index(QtCore.QDir.currentPath() + "/database"))
        self.setWidget(self.tree)
        self.tree.clicked.connect(self.item_model)

    def item_selected(self, index): 
        try:
            repo = self.get_repository(index.data())

            if repo is None:
                self.get_metadata()

            self.main_window.open_sql_table(repo)
        except Exception as e:
            print(e)
    
    def get_repository(self, repo_name):
        for table in self.metadata["tables"]:
            if repo_name == table["name"]:
                return DatabaseGeneric(table["name"], table["key"], self.metapath)
            
                