from PySide2 import QtWidgets, QtGui, QtCore
from view.structure_dock import StructureDock
from view.tool_bar import ToolBar
from view.status_bar import StatusBar
from util.icon import Icon
from PySide2.QtWidgets import QMainWindow, QMessageBox
from PySide2.QtCore import QEvent
from PySide2.QtGui import QIcon
from view.workspace_widget import WorkspaceWidget
from view.create_dialog import CreateDialog
from util.serial_file_handler import SerialFileHandler
from util.sequential_file_handler import SequentialFileHandler
import json
import os
from view.database_structure_dock import DatabaseStructureDock
from view.database_workspace import DatabaseWorkspace
from util.db_handler import DatabaseHandler

#Definicija glavnog prozora
class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.dbwork = DatabaseWorkspace(self)
        #Podesavanja glavnog prozora
        self.resize(640, 480)
        self.setWindowTitle("Editor generickih podataka")
        self.setWindowIcon(QtGui.QIcon(Icon.APP_LOGO))
        self.showMaximized()

        self.structure_dock = StructureDock("Data files", self)
        self.workspace = WorkspaceWidget(self)
        self.status_bar = QtWidgets.QStatusBar(self)
        self.tool_bar = ToolBar(self)
        # self.status_bar.showMessage("Status prikaazan")

        # Database def
        self.structure_dock_database = DatabaseStructureDock("Database", self)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.structure_dock_database)

        #Definicija menu_bar-a, i dodavanje menija
        menu_bar = QtWidgets.QMenuBar(self)
        
        #Definicija file_menu-a
        file_menu = QtWidgets.QMenu("File", menu_bar)
        new_button = self.createQAction(self, "Nova datoteka", "Ctrl+N", "Kreiraj novu datoteku", self.create_new_table)
        open_button = self.createQAction(self, "Otvori folder", "Ctrl+O", "Otvori radni prostor", self.structure_dock.openFolder)
        save_button = self.createQAction(self, "Sacuvaj", "Ctrl+S", "Sacuvaj trenutno otvorenu datoteku", self.workspace.save)
        quit_button = self.createQAction(self, "Izlaz", "Ctrl+Q", "Izlaz iz aplikacije", self.close)
        file_menu.addAction(new_button)
        file_menu.addAction(open_button)
        file_menu.addSeparator()
        file_menu.addAction(save_button)
        file_menu.addSeparator()
        file_menu.addAction(quit_button)

        #Definicija edit_menu-a
        edit_menu = QtWidgets.QMenu("Edit", menu_bar)
        new_row_button = self.createQAction(self, "Novi red", "Insert", "Dodaje novi red u tabeli", self.workspace.new_row)
        delete_row_button = self.createQAction(self, "Obrisi red", "Delete", "Obrise red ili redove u tabeli", self.workspace.delete_row)
        edit_menu.addAction(new_row_button)
        edit_menu.addAction(delete_row_button)

        #Definicija view_menu-a
        view_menu = QtWidgets.QMenu("View", menu_bar)

        #Definicija help_menu-a
        help_menu = QtWidgets.QMenu("Help", menu_bar)

        menu_bar.addMenu(file_menu)
        menu_bar.addMenu(edit_menu)
        menu_bar.addMenu(view_menu)
        menu_bar.addMenu(help_menu)

        #Definicija status_bar-a
        # status_bar = StatusBar(self)

        # #Definicija centralnog widget-a
        # central_widget = CentralWidget(self, status_bar)

        # #Definicija toolbar-a
        # tool_bar = ToolBar("Default actions", central_widget, status_bar)

        # #Definicija structure dock-a
        # structure_dock = StructureDock("Data files", self, central_widget)

        # toggle_structure_dock_action = structure_dock.toggleViewAction()
        # view_menu.addAction(toggle_structure_dock_action)

        #Dodavanje svih kreiranih widget-a i menija u MainWindow da se prikazu
        self.setCentralWidget(self.workspace)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.structure_dock)
        self.setStatusBar(self.status_bar)
        self.addToolBar(self.tool_bar)
        self.setMenuBar(menu_bar)

    def set_workspace(self):
        self.central_widget = QtWidgets.QTabWidget(self)
        self.workspace = WorkspaceWidget(self.central_widget)
        self.central_widget.addTab(self.workspace, QtGui.QIcon(Icon.APP_LOGO), "Prikaz podataka iz datoteke")
        self.central_widget.setTabsClosable(True)
        self.setCentralWidget(self.central_widget)

    def open_sql_table(self, filehandler):
        self.workspace.load_table_model(filehandler, None)

    # Funkcija za kreiranje QAction-a
    def createQAction(self, parent, text, shortcut, statusTip, connectFunction):
        action = QtWidgets.QAction(text, parent)
        action.setShortcut(shortcut)
        action.setStatusTip(statusTip)
        action.triggered.connect(connectFunction)
        return action

    def closeEvent(self, event):
        exit = QMessageBox()
        exit.setWindowTitle("Exit")
        exit.setText("Da li ste sigurni da zelite da napustite aplikaciju?")
        exit.setStandardButtons(QMessageBox.No | QMessageBox.Yes)
        exit.setIcon(QMessageBox.Information)

        den = exit.exec_()

        if den == QMessageBox.Yes:
            event.accept()
        elif den == QMessageBox.No:
            event.ignore()
            return

    def create_new_table(self):
        result, path = CreateDialog.create_new_table(self)
        if result == 1:
            self.structure_dock.openFolder(os.path.dirname(path))
            self.open_workspace(path)
        
    def open_workspace(self, filepath):
        meta_path = filepath.replace(".data", "_metadata.json")
        
        with open(meta_path) as meta:
            metadata = json.load(meta)
            
        file_handler = self.get_file_handler(metadata["type"], filepath, meta_path)

        linked_file_handlers = []
        linked_files = metadata['linked_files']
        if len(linked_files) == 0:
            print("nema povezanih")
        else:
            linked_path = filepath[:filepath.rindex("/")]

            for i in range(len(linked_files)):
                linked_data_file_path = linked_path + "/" + linked_files[i]
                linked_meta_data_file_path = linked_data_file_path.replace(".data", "_metadata.json")
                with open(linked_meta_data_file_path) as linked_meta:
                    linked_meta = json.load(linked_meta)
                
                try:
                    linked_file_handler = self.get_file_handler(linked_meta["type"], linked_data_file_path, linked_meta_data_file_path, True)
                except Exception as ex:
                    errorMsg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Greska u otvaranju datoteke", "Greska u otvaranju povezane datoteke:\n" + linked_data_file_path + "\n\n" + str(ex) + ".")
                    errorMsg.exec()
                    return
                
                linked_file_handlers.append(linked_file_handler)

        self.workspace.load_table_model(file_handler, linked_file_handlers)
        self.status_bar.showMessage("Read data from '" + os.path.basename(filepath) + "'")
    
    def get_file_handler(self, handler_type, data_path, meta_path, readonly=False):
        if handler_type == "serijska":
            return SerialFileHandler(data_path, meta_path, readonly)
        elif handler_type == "sekvencijalna":
            return SequentialFileHandler(data_path, meta_path, readonly)
        else:
            raise Exception("Nije podrzan tip file handler-a")
    
    # Pokusa da doda novi tab u centralni widget, ako je datoteka vec otvorena onda je fokusira
    def add_tab(self, widget, icon, label):
        for i in range(len(self.openTabs)):
            if label == self.openTabs[i]:
                self.setCurrentIndex(i)
                return
        
        self.openTabs.append(label)
        self.addTab(widget, icon, label)
        self.setCurrentIndex(self.count()-1)

    def remove_tab(self, index):
        self.removeTab(index)
        del self.openTabs[index]
        self.status_bar.showMessage("")