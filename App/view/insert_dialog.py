from PySide2 import QtWidgets, QtGui, QtCore
from util.icon import Icon

#TODO: da se ne izadje iz dialoga ako je neuspesno dodavanje

class InsertDialog(QtWidgets.QDialog):
    def __init__(self, parent, table_model, file_handler):
        super(InsertDialog, self).__init__(parent, QtCore.Qt.Dialog)
        self.setWindowTitle("Novi red")
        self.setWindowIcon(QtGui.QIcon(Icon.INSERT_ROW))

        self.table_model = table_model
        self.file_handler = file_handler
        self.key_column = None
        self.textBoxes = []
        self.keyId = None
        self.workspace = parent

        self.resultValue = False
        self.dontclose = False

        layout = QtWidgets.QVBoxLayout()

        # Dodavanje textbox-a za svaku kolonu
        for i in range(len(table_model.get_column_names())):
            isKey = False
            if self.file_handler.get_type() != "serijska" and table_model.get_column_names()[i] == table_model.get_key():
                isKey = True
                self.keyId = i
                self.key_column = table_model.get_column_names()[i]

            columnTitle = table_model.get_column_names()[i].replace("_", " ")
            if columnTitle[0].islower():
                columnTitle = columnTitle.capitalize()

            hbox = QtWidgets.QHBoxLayout()
            hbox.addStretch(10)

            if isKey and self.file_handler.get_type() == "sekvencijalna":
                keyImage = QtWidgets.QLabel(self)
                pixmap = QtGui.QPixmap(Icon.KEY)
                keyImage.setPixmap(pixmap)
                keyImage.setWhatsThis("Kljucno polje.")
                hbox.addWidget(keyImage)

            text = QtWidgets.QLabel(columnTitle)

            lineEdit = QtWidgets.QLineEdit()
            lineEdit.setPlaceholderText(columnTitle)
            self.textBoxes.append(lineEdit)
            hbox.addWidget(text)
            hbox.addWidget(lineEdit)
            layout.addLayout(hbox)

        addrow_button = QtWidgets.QPushButton("Dodaj novu kolonu")
        addrow_button.clicked.connect(self.create_row)
        addrow_button.clicked.connect(self.accept)
        layout.addWidget(addrow_button)

        self.setLayout(layout)

    # Privatna funkcija koja pokusa da kreira red
    def create_row(self):
        # Provera da li je unesena key vrednost (obavezno)
        if self.keyId != None:
            if self.textBoxes[self.keyId].text() == "":
                msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Greska pri unosu", "Morate uneti kljucnu vrednost!")
                msg.exec()
                self.dontclose = True
                return

        # Kreiranje klase koristeci metapodatke
        metadict = {}
        
        model = self.table_model
        # Popunjavanje prethodno generisane klase
        i = 0
        for column in self.file_handler.get_column_names():
            metadict[column] = self.textBoxes[i].text()
            i += 1

        try:
            self.file_handler.insert(metadict)
        except Exception as ex:
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Neuspesno dodavanje", str(ex))
            msg.exec()

    def closeEvent(self, event):
        if self.dontclose:
            event.ignore()
            self.dontclose = False
        else:
            event.accept()

    def getResult(self):
        return self.resultValue

    # Javna metoda za otvaranje dialoga i vracanje rezultata
    @staticmethod
    def create_new_row(parent, table_model, file_handler):
        dialog = InsertDialog(parent, table_model, file_handler)
        dialog_result = dialog.exec()
        return dialog_result
