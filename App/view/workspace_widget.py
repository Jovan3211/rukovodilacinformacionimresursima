from PySide2 import QtWidgets, QtGui
from util.icon import Icon
from view.table_widget import TableWidget
from view.workspace_container import WorkspaceContainer
import os

#Klasa za glavni radni prostor programa
class WorkspaceWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_layout = QtWidgets.QVBoxLayout()

        # Konstruisanje maintable_tab_widget-a
        self.table_tab_widget = QtWidgets.QTabWidget(self)
        self.table_tab_widget.setTabsClosable(True)
        self.table_tab_widget.tabCloseRequested.connect(self.delete_tab)

        # Dodavanje elemenata na main_layout
        self.main_layout.addWidget(self.table_tab_widget)
        self.setLayout(self.main_layout)

    def load_table_model(self, file_handler, subtable_handlers):
        filename = None
        if file_handler.get_type() == "baza":
            print("Workspace: LoadTableModel: Ucitavanje tabele baze")
            filename = file_handler.table
        else:
            filename = os.path.basename(file_handler.filePath)

        # Provera da li je vec otvorena tabela
        # Ako jeste fokusiraj taj tab
        # TODO: Trenutno ako se otvori file u dva razlicita foldera sa istim imenom,
        #       fokusirace na taj file koji je prvi otvoren, iako je moguce da se razlikuju
        for index in range(self.table_tab_widget.count()):
            if self.table_tab_widget.tabText(index) == filename:
                self.table_tab_widget.setCurrentIndex(index)
                return

        # Kreiranje novog workspace_layout-a i dodavanje na novi tab
        workspace_container = WorkspaceContainer(file_handler, subtable_handlers)

        self.table_tab_widget.addTab(workspace_container, Icon.get_icon_for_filetype(file_handler), filename)
        self.table_tab_widget.setCurrentIndex(self.table_tab_widget.count()-1)

    # Sacuva sve otvorene tabele i podtabele (poziva save na workspace containers)
    def save(self):
        for i in range(self.table_tab_widget.count()):
            self.table_tab_widget.widget(i).save()

    # Poziva new_row() funkciju na trenutno otvoren workspace_container
    def new_row(self):
        self.table_tab_widget.currentWidget().new_row()

    # TODO: Na trenutno fokusiranu tabelu (pod ili glavnu)
    #       obrisati selektovane redove
    def delete_row(self):
        print("workspace delete row")

    def delete_tab(self, index):
        workspace = self.table_tab_widget.widget(index)
        workspace.close_workspace()
        self.table_tab_widget.removeTab(index)