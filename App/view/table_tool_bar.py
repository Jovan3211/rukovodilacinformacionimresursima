from PySide2 import QtWidgets, QtGui, QtCore
from util.icon import Icon

#Toolbar za funkcije tabele
class TableToolBar(QtWidgets.QToolBar):
    def __init__(self, parent, noedit, nosave):
        super().__init__(parent)

        if not noedit:
            if not nosave:
                self.addAction(QtGui.QIcon(Icon.SAVE), "Sacuvaj", parent.save)
            self.addAction(QtGui.QIcon(Icon.INSERT_ROW), "Umetni red", parent.new_row)
            self.addAction(QtGui.QIcon(Icon.DELETE_ROW), "Obrisi red", parent.delete_row)
        self.addAction(QtGui.QIcon(Icon.SEARCH), "Pretraga", parent.search)
        self.addAction(QtGui.QIcon(Icon.RESET), "Resetuj prikaz", parent.refresh_model)