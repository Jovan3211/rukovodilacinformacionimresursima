from PySide2 import QtWidgets, QtGui, QtCore
from util.icon import Icon

#Toolbar za funkcije tabele
class DatabaseToolBar(QtWidgets.QToolBar):
    def __init__(self, parent):
        super().__init__(parent)

        #self.addAction(QtGui.QIcon(Icon.SAVE), "Sacuvaj", parent.save)
        self.addAction(QtGui.QIcon(Icon.INSERT_ROW), "Dodaj red u bazu", parent.insert_form)
        self.addAction(QtGui.QIcon(Icon.DELETE_ROW), "Obrisi red iz baze", parent.delete_form)
        self.addAction(QtGui.QIcon(Icon.SEARCH), "Pretrazi", parent.search_form)