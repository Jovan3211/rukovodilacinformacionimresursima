from PySide2 import QtWidgets, QtCore

class SearchWidget(QtWidgets.QWidget):
    def __init__(self, file_handler, toolbar, parent=None):
        super().__init__(parent)

        self.parent = parent
        self.file_handler = file_handler
        
        # Searchbox hbox
        self.search_container = QtWidgets.QHBoxLayout()
        self.search_container.setAlignment(QtCore.Qt.AlignLeft)

        # Search dialog
        self.search_dialog = None
        self.textboxes = {}

        # Dodavanje toolbar-a (on sadrzi samo akcije i dugma) u widget
        self.search_container.addWidget(toolbar)

        # Podesavanje layout-a
        self.setLayout(self.search_container)

    def make_search_dialog(self):
        dialog = QtWidgets.QDialog()
        button = QtWidgets.QPushButton("Pretraga")
        layout = QtWidgets.QVBoxLayout()

        for column in self.file_handler.get_column_names():
            horizontal = QtWidgets.QHBoxLayout()
            label = QtWidgets.QLabel(column)
            textbox = QtWidgets.QLineEdit()
            self.textboxes[column] = textbox
            horizontal.addWidget(label)
            horizontal.addStretch(300)
            horizontal.addWidget(textbox)

            layout.addLayout(horizontal)
            searching = column
        
        layout.addWidget(button)
        dialog.setWindowTitle("Pretraga")
        dialog.setWhatsThis("Unesite vrednosti kolona za pretragu")
        dialog.setLayout(layout)

        button.clicked.connect(self.do_search)
        return dialog

    def do_search(self):
        arguments = {}
        for column, textbox in self.textboxes.items():
            arguments[column] = textbox.text()
        result = self.file_handler.search(arguments)
        self.parent.refresh_model(result)

    def search(self):
        self.search_dialog = self.make_search_dialog()
        self.search_dialog.show()
