from PySide2 import QtWidgets, QtGui, QtCore
from util.serial_file_handler import SerialFileHandler
from util.paths import Paths
from util.icon import Icon
import os

#Definicija prikaza strukturnog dock-a sa leve strane programa
class StructureDock(QtWidgets.QDockWidget):
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.main_window = parent
        self.openFolder(QtCore.QDir.currentPath() + Paths.path_data)

    def openFolder(self, path=None):
        # Ako nije navedena putanja onda otvori dialog za biranje direktorijuma
        if path == None or path == False:
            path = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Odaberite radni direktorijum"))

        data_extension = ["*.data", "*.txt"]

        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath(path)
        self.model.setFilter(QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot | QtCore.QDir.AllEntries)
        self.model.setNameFilters(data_extension)
        self.model.setNameFilterDisables(False)
        
        self.tree = QtWidgets.QTreeView()
        self.tree.setModel(self.model)
        self.tree.setRootIndex(self.model.index(path))
        self.tree.clicked.connect(self.clickAction)

        self.setWidget(self.tree)

    # Akcija pozvana klikom fajla ili foldera u stablu
    # Uzima putanju fajla i pokusava da otvori datoteku
    def clickAction(self, index):
        self.file_path = self.model.filePath(index)
        self.meta_file_path = self.file_path.replace(".data", "_metadata.json")
        print("structure_dock.py: Kliknuta datoteka, " + self.file_path + " " + self.meta_file_path)
        self.main_window.open_workspace(self.file_path)


