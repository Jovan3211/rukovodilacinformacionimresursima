from PySide2 import QtWidgets, QtGui
from util.icon import Icon
from view.table_widget import TableWidget
import os

# Container koji sadrzi glavnu tabelu, podtabele, i rukovodi njima
class WorkspaceContainer(QtWidgets.QWidget):
    def __init__(self, file_handler, subtable_handlers, parent=None):
        super().__init__(parent)
        workspace_layout = QtWidgets.QVBoxLayout(parent)

        self.subtable_handlers = subtable_handlers

        self.table = TableWidget(file_handler)
        workspace_layout.addWidget(self.table)
        
        if self.subtable_handlers != None:
            # Konstruisanje subtable_tab_widget-a
            self.subtable_tab_widget = QtWidgets.QTabWidget()
            self.subtable_tab_widget.setTabsClosable(True)
            self.subtable_tab_widget.tabCloseRequested.connect(self.delete_subtable_tab)

            workspace_layout.addWidget(self.subtable_tab_widget)

            # Dodavanje tabova svih pod-tabela
            for subhandler in self.subtable_handlers:
                subtable = TableWidget(subhandler, self.subtable_tab_widget)
                self.subtable_tab_widget.addTab(subtable, Icon.get_icon_for_filetype(subhandler), os.path.basename(subhandler.filePath))

        self.setLayout(workspace_layout)

    def close_workspace(self):
        self.table.close()
        if self.subtable_handlers != None:
            for i in range(self.subtable_tab_widget.count()):
                subtable = self.subtable_tab_widget.widget(i)
                subtable.close()

    def delete_subtable_tab(self, index):
        if self.subtable_handlers == None:
            return
        subtable = self.subtable_tab_widget.widget(index)
        subtable.close()
        self.subtable_tab_widget.removeTab(index)

    # Funkcija koja poziva save() funkcije na svojoj glavnoj tabeli i svim pod-tabelama
    def save(self):
        self.table.save()
        if self.subtable_handlers != None:
            for i in range(self.subtable_tab_widget.count()):
                self.subtable_tab_widget.widget(i).save()

    # Funkcija poziva new_row() od svoje glavne tabele
    def new_row(self):
        self.table.new_row()