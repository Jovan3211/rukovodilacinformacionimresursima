import sys
from application import Application

#Glavne linije programa
if __name__ == "__main__":
    app = Application()
    app.run()
    sys.exit(app.exec_())
