import sys
from PySide2 import QtWidgets, QtGui, QtCore
from view.main_window import MainWindow

class Application(QtWidgets.QApplication):
    def __init__(self):
        super().__init__(sys.argv)

    def run(self):
        main_window = MainWindow()
        main_window.show()