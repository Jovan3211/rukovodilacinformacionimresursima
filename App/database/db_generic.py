from util.db_handler import DatabaseHandler

class DatabaseGeneric(DatabaseHandler):
    def __init__(self, table, key, metapath):
        self.table = table
        self.key = key
        self.metapath = metapath

        super().__init__(table, key, metapath)