CREATE DATABASE  IF NOT EXISTS `visokoskolska_ustanova` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `visokoskolska_ustanova`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: visokoskolska_ustanova
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `nastavni_predmet`
--

DROP TABLE IF EXISTS `nastavni_predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nastavni_predmet` (
  `oznaka` varchar(6) NOT NULL,
  `visokoskolska_ustanova` char(2) NOT NULL,
  `naziv` varchar(120) NOT NULL,
  `espb` decimal(2,0) NOT NULL,
  PRIMARY KEY (`oznaka`),
  KEY `fk_nastavni_predmet_visokoskolska_ustanova1_idx` (`visokoskolska_ustanova`),
  CONSTRAINT `fk_nastavni_predmet_visokoskolska_ustanova1` FOREIGN KEY (`visokoskolska_ustanova`) REFERENCES `visokoskolska_ustanova` (`oznaka`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastavni_predmet`
--

LOCK TABLES `nastavni_predmet` WRITE;
/*!40000 ALTER TABLE `nastavni_predmet` DISABLE KEYS */;
INSERT INTO `nastavni_predmet` VALUES ('BP','SG','Baze podataka',8),('H','PM','Hemija',6),('M','TN','Matematika',6),('PL','FF','Psihologija licnosti',8);
/*!40000 ALTER TABLE `nastavni_predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nivo_studija`
--

DROP TABLE IF EXISTS `nivo_studija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nivo_studija` (
  `oznaka` decimal(2,0) NOT NULL,
  `naziv` varchar(80) NOT NULL,
  PRIMARY KEY (`oznaka`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivo_studija`
--

LOCK TABLES `nivo_studija` WRITE;
/*!40000 ALTER TABLE `nivo_studija` DISABLE KEYS */;
INSERT INTO `nivo_studija` VALUES (1,'Fakultet'),(2,'Master'),(3,'Doktorat');
/*!40000 ALTER TABLE `nivo_studija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plan_studijske_grupe`
--

DROP TABLE IF EXISTS `plan_studijske_grupe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plan_studijske_grupe` (
  `oznaka_programa` varchar(3) NOT NULL,
  `studijski_program` varchar(3) NOT NULL,
  `blok` decimal(2,0) NOT NULL,
  `pozicija` decimal(2,0) NOT NULL,
  `ustanova_predmet` char(2) NOT NULL,
  `nastavni_predmet` varchar(6) NOT NULL,
  PRIMARY KEY (`oznaka_programa`),
  KEY `fk_plan_studijske_grupe_studijski_program2_idx` (`studijski_program`),
  KEY `fk_plan_studijske_grupe_nastavni_predmet1_idx` (`nastavni_predmet`),
  CONSTRAINT `fk_plan_studijske_grupe_nastavni_predmet1` FOREIGN KEY (`nastavni_predmet`) REFERENCES `nastavni_predmet` (`oznaka`),
  CONSTRAINT `fk_plan_studijske_grupe_studijski_program1` FOREIGN KEY (`oznaka_programa`) REFERENCES `studijski_program` (`oznaka_programa`),
  CONSTRAINT `fk_plan_studijske_grupe_studijski_program2` FOREIGN KEY (`studijski_program`) REFERENCES `studijski_program` (`oznaka_programa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan_studijske_grupe`
--

LOCK TABLES `plan_studijske_grupe` WRITE;
/*!40000 ALTER TABLE `plan_studijske_grupe` DISABLE KEYS */;
INSERT INTO `plan_studijske_grupe` VALUES ('IT','IT',2,5,'BP','BP'),('SI','SI',1,2,'M','M');
/*!40000 ALTER TABLE `plan_studijske_grupe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studenti`
--

DROP TABLE IF EXISTS `studenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `studenti` (
  `broj_indeksa` varchar(6) NOT NULL,
  `visokoskolska_ustanova` char(2) NOT NULL,
  `struka` char(2) NOT NULL,
  `prezime` varchar(20) NOT NULL,
  `ime_roditelja` varchar(20) DEFAULT NULL,
  `ime` varchar(20) NOT NULL,
  `pol` char(1) NOT NULL,
  `adresa_stanovanja` varchar(80) DEFAULT NULL,
  `telefon` varchar(20) DEFAULT NULL,
  `jmbg` char(13) DEFAULT NULL,
  `datum_rodjenja` date DEFAULT NULL,
  PRIMARY KEY (`broj_indeksa`),
  KEY `fk_studenti_visokoskolska_ustanova_idx` (`visokoskolska_ustanova`),
  CONSTRAINT `fk_studenti_visokoskolska_ustanova` FOREIGN KEY (`visokoskolska_ustanova`) REFERENCES `visokoskolska_ustanova` (`oznaka`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studenti`
--

LOCK TABLES `studenti` WRITE;
/*!40000 ALTER TABLE `studenti` DISABLE KEYS */;
INSERT INTO `studenti` VALUES ('270010','SG','I','Milanic','Petar','Milan','M','Bulevar oslobodjenja','555333','0505999555023','1999-05-05'),('270402','FF','P','Dragic','Dejan','Drago','M','Pap Pavla','222333','1103000500234','2000-11-03');
/*!40000 ALTER TABLE `studenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studijski_program`
--

DROP TABLE IF EXISTS `studijski_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `studijski_program` (
  `oznaka_programa` varchar(3) NOT NULL,
  `nivo_studija` decimal(2,0) NOT NULL,
  `visokoskolska_ustanova` char(2) NOT NULL,
  `naziv_programa` varchar(120) NOT NULL,
  PRIMARY KEY (`oznaka_programa`),
  KEY `fk_studijski_program_nivo_studija1_idx` (`nivo_studija`),
  KEY `fk_studijski_program_visokoskolska_ustanova1_idx` (`visokoskolska_ustanova`),
  CONSTRAINT `fk_studijski_program_nivo_studija1` FOREIGN KEY (`nivo_studija`) REFERENCES `nivo_studija` (`oznaka`),
  CONSTRAINT `fk_studijski_program_visokoskolska_ustanova1` FOREIGN KEY (`visokoskolska_ustanova`) REFERENCES `visokoskolska_ustanova` (`oznaka`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studijski_program`
--

LOCK TABLES `studijski_program` WRITE;
/*!40000 ALTER TABLE `studijski_program` DISABLE KEYS */;
INSERT INTO `studijski_program` VALUES ('BE',2,'PM','Biologija i ekologija'),('IT',1,'SG','Informacione tehnologije'),('P',1,'FF','Psihologija'),('SI',2,'TN','Softversko inzenjerstvo');
/*!40000 ALTER TABLE `studijski_program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tok_studija`
--

DROP TABLE IF EXISTS `tok_studija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tok_studija` (
  `studenti_broj_indeksa` varchar(6) NOT NULL,
  `ustanova` char(2) NOT NULL,
  `studijski_program` varchar(3) NOT NULL,
  `studenti_iz_ustanove` char(2) NOT NULL,
  `struka` char(2) NOT NULL,
  `skolska_godina` decimal(4,0) NOT NULL,
  `godina_studija` decimal(1,0) NOT NULL,
  `blok` decimal(2,0) NOT NULL,
  `redni_broj_upisa` decimal(2,0) NOT NULL,
  `datum_upisa` date NOT NULL,
  `datum_overe` date DEFAULT NULL,
  `espb_pocetni` decimal(3,0) NOT NULL,
  `espb_krajnji` decimal(3,0) NOT NULL,
  PRIMARY KEY (`studenti_broj_indeksa`),
  KEY `fk_tok_studija_studijski_program1_idx` (`studijski_program`),
  CONSTRAINT `fk_tok_studija_studenti1` FOREIGN KEY (`studenti_broj_indeksa`) REFERENCES `studenti` (`broj_indeksa`),
  CONSTRAINT `fk_tok_studija_studijski_program1` FOREIGN KEY (`studijski_program`) REFERENCES `studijski_program` (`oznaka_programa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tok_studija`
--

LOCK TABLES `tok_studija` WRITE;
/*!40000 ALTER TABLE `tok_studija` DISABLE KEYS */;
INSERT INTO `tok_studija` VALUES ('270010','SG','IT','SG','I',1,1,2,10,'2018-03-21','2018-03-25',0,240),('270402','FF','P','FF','P',2,2,2,3,'2017-03-20','2018-03-21',0,240);
/*!40000 ALTER TABLE `tok_studija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visokoskolska_ustanova`
--

DROP TABLE IF EXISTS `visokoskolska_ustanova`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visokoskolska_ustanova` (
  `oznaka` char(2) NOT NULL,
  `naziv` varchar(80) NOT NULL,
  `adresa` varchar(80) NOT NULL,
  PRIMARY KEY (`oznaka`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visokoskolska_ustanova`
--

LOCK TABLES `visokoskolska_ustanova` WRITE;
/*!40000 ALTER TABLE `visokoskolska_ustanova` DISABLE KEYS */;
INSERT INTO `visokoskolska_ustanova` VALUES ('FF','Filozofski fakultet','Kumodraska 8'),('PM','Prirodno-matematicki fakultet','Pap Pavla 12'),('SG','Singidunum','Danijelova 32'),('TN','Fakultet tehnickih nauka','Novosadska 15');
/*!40000 ALTER TABLE `visokoskolska_ustanova` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'visokoskolska_ustanova'
--
/*!50003 DROP PROCEDURE IF EXISTS `np_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `np_create`(IN nova_visokoskolska_ustanova VARCHAR(32), IN nova_oznaka VARCHAR(32), IN novi_naziv VARCHAR(32), IN novi_espb VARCHAR(32))
BEGIN
	INSERT INTO nastavni_predmet(visokoskolska_ustanova, oznaka, naziv, espb) values(nova_visokoskolska_ustanova, nova_oznaka, novi_naziv, novi_espb);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `np_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `np_delete`(IN id VARCHAR(32))
BEGIN
	DELETE FROM nastavni_predmet
    WHERE oznaka = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `np_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `np_read`()
BEGIN
	SELECT * FROM nastavni_predmet;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `np_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `np_search`(IN trazi_ustanovu VARCHAR(32), IN trazi_oznaku VARCHAR(32), IN trazi_naziv VARCHAR(32), IN trazi_espb VARCHAR(32))
BEGIN
	SELECT * FROM nastavni_predmet
    WHERE visokoskolska_ustanova = trazi_ustanovu OR oznaka = trazi_oznaku OR naziv = trazi_naziv OR espb = trazi_espb ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `np_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `np_update`(IN nova_ustanova VARCHAR(32), IN nova_oznaka VARCHAR(32), IN novi_naziv VARCHAR(32), IN novi_espb VARCHAR(32))
BEGIN
 UPDATE nastavni_predmet
 SET visokoskolska_ustanova = nova_ustanova, naziv = novi_naziv, espb = novi_espb
 WHERE oznaka = nova_oznaka;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ns_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ns_create`(IN nova_oznaka VARCHAR(32), IN novi_naziv VARCHAR(32))
BEGIN
	INSERT INTO nivo_studija(oznaka, naziv) values(nova_oznaka, novi_naziv);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ns_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ns_delete`(IN id VARCHAR(32))
BEGIN
	DELETE FROM nivo_studija
    WHERE oznaka = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ns_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ns_read`()
BEGIN
	SELECT * FROM nivo_studija;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ns_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ns_search`(IN trazi_oznaku VARCHAR(32), IN trazi_naziv VARCHAR(32))
BEGIN
	SELECT * FROM nivo_studija
    WHERE oznaka = trazi_oznaku OR naziv = trazi_naziv;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ns_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ns_update`(IN nova_oznaka VARCHAR(32), IN novi_naziv VARCHAR(32))
BEGIN
 UPDATE nivo_studija
 SET naziv = novi_naziv
 WHERE oznaka = nova_oznaka;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `psg_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `psg_create`(IN novi_studijski_program VARCHAR(32), IN nova_oznaka VARCHAR(32), IN novi_blok VARCHAR(32), IN nova_pozicija VARCHAR(32), IN nova_ustanova_predmet VARCHAR(32), IN novi_nastavni_predmet VARCHAR(32))
BEGIN
	INSERT INTO plan_studijske_grupe(studijski_program, oznaka_programa, blok, pozicija, ustanova_predmet, nastavni_predmet) values(novi_studijski_program, nova_oznaka, novi_blok, nova_pozicija, nova_ustanova_predmet, novi_nastavni_predmet);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `psg_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `psg_delete`(IN id VARCHAR(32))
BEGIN
	DELETE FROM plan_studijske_grupe
    WHERE oznaka_programa = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `psg_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `psg_read`()
BEGIN
	SELECT * FROM plan_studijske_grupe;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `psg_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `psg_search`(IN trazi_program VARCHAR(32), IN trazi_oznaku VARCHAR(32), IN trazi_blok VARCHAR(32), IN trazi_poziciju VARCHAR(32), IN trazi_ustanovu VARCHAR(32), IN trazi_nastavni_predmet VARCHAR(32))
BEGIN
	SELECT * FROM plan_studijske_grupe
    WHERE studijski_program = trazi_program OR oznaka_programa = trazi_oznaku OR blok = trazi_blok OR pozicija = trazi_poziciju OR ustanova_predmet = trazi_ustanovu OR nastavni_predmet = trazi_nastavni_predmet;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `psg_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `psg_update`(IN novi_studijski_program VARCHAR(32), IN nova_oznaka VARCHAR(32), IN novi_blok VARCHAR(32), IN nova_pozicija VARCHAR(32), IN nova_ustanova_predmet VARCHAR(32), IN novi_nastavni_predmet VARCHAR(32))
BEGIN
 UPDATE plan_studijske_grupe
 SET studijski_program = novi_studijski_program, blok = novi_blok, pozicija = nova_pozicija, ustanova_predmet = nova_ustanova_predmet, nastavni_predmet = novi_nastavni_predmet
 WHERE oznaka_programa = nova_oznaka;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_create`(IN novi_nivo_studija VARCHAR(32), IN nova_visokoskolska_ustanova VARCHAR(32), IN nova_oznaka_programa VARCHAR(32), IN novi_naziv_programa VARCHAR(32))
BEGIN
INSERT INTO studijski_program(nivo_studija, visokoskolska_ustanova, oznaka_programa, naziv_programa) values(novi_nivo_studija, nova_visokoskolska_ustanova, nova_oznaka_programa, novi_naziv_programa);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete`(IN id VARCHAR(32))
BEGIN
	DELETE FROM studijski_program
    WHERE oznaka_programa = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_read`()
BEGIN
	SELECT * FROM studijski_program;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_search`(IN trazi_nivo_studija VARCHAR(32), IN trazi_ustanovu VARCHAR(32), IN trazi_oznaku VARCHAR(32), IN trazi_naziv_programa VARCHAR(32))
BEGIN
	SELECT * FROM studijski_program
    WHERE nivo_studija = trazi_nivo_studija OR visokoskolska_ustanova = trazi_ustanovu OR oznaka_programa = trazi_oznaku OR naziv_programa = trazi_naziv_programa ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update`(IN novi_nivo_studija VARCHAR(32), IN nova_visokoskolska_ustanova VARCHAR(32), IN nova_oznaka_programa VARCHAR(32), IN novi_naziv_programa VARCHAR(32))
BEGIN
 UPDATE studijski_program
 SET nivo_studija = novi_nivo_studija, visokoskolska_ustanova = nova_visokoskolska_ustanova, naziv_programa = novi_naziv_programa
 WHERE oznaka_programa = nova_oznaka_programa;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `s_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `s_create`(IN nova_visokoskolska_ustanova VARCHAR(32), IN nova_struka VARCHAR(32), IN novi_br_indeksa VARCHAR(32), IN novo_prezime VARCHAR(32), IN novo_ime_roditelja VARCHAR(32), IN novo_ime VARCHAR(32), IN novi_pol VARCHAR(32), IN nova_adresa VARCHAR(32), IN novi_telefon VARCHAR(32), IN novi_jmbg VARCHAR(32), IN novi_datum_rodjenja VARCHAR(32))
BEGIN
	INSERT INTO studenti(visokoskolska_ustanova, struka, broj_indeksa, prezime, ime_roditelja, ime, pol, adresa_stanovanja, telefon, jmbg, datum_rodjenja) values(nova_visokoskolska_ustanova, nova_struka, novi_br_indeksa, novo_prezime, novo_ime_roditelja, novo_ime, novi_pol, nova_adresa, novi_telefon, novi_jmbg, novi_datum_rodjenja);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `s_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `s_delete`(IN id VARCHAR(32))
BEGIN
	DELETE FROM studenti
    WHERE broj_indeksa = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `s_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `s_read`()
BEGIN
	SELECT * FROM studenti;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `s_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `s_search`(IN trazi_ustanovu VARCHAR(32), IN trazi_struku VARCHAR(32), IN trazi_broj_indeksa VARCHAR(32), IN trazi_prezime VARCHAR(32), IN trazi_ime_roditelja VARCHAR(32), IN trazi_ime VARCHAR(32), IN trazi_pol VARCHAR(32), IN trazi_adresu VARCHAR(32), IN trazi_telefon VARCHAR(32), IN trazi_jmbg VARCHAR(32), IN trazi_datum_rodjenja VARCHAR(32))
BEGIN
	SELECT * FROM studenti
    WHERE visokoskolska_ustanova = trazi_ustanovu OR struka = trazi_struku OR broj_indeksa = trazi_broj_indeksa OR prezime = trazi_prezime OR ime_roditelja = trazi_ime_roditelja OR ime = trazi_ime OR pol = trazi_pol OR adresa_stanovanja = trazi_adresu OR telefon = trazi_telefon OR jmbg = trazi_jmbg OR datum_rodjenja = trazi_datum_rodjenja;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `s_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `s_update`(IN nova_visokoskolska_ustanova VARCHAR(32), IN nova_struka VARCHAR(32), IN novi_br_indeksa VARCHAR(32), IN novo_prezime VARCHAR(32), IN novo_ime_roditelja VARCHAR(32), IN novo_ime VARCHAR(32), IN novi_pol VARCHAR(32), IN nova_adresa VARCHAR(32), IN novi_telefon VARCHAR(32), IN novi_jmbg VARCHAR(32), IN novi_datum_rodjenja VARCHAR(32))
BEGIN
 UPDATE studenti
 SET visokoskolska_ustanova = nova_visokoskolska_ustanova, struka = nova_struka, broj_indeksa = novi_br_indeksa, prezime = novo_prezime, ime_roditelja = novo_ime_roditelja, ime = novo_ime, pol = novi_pol, adresa_stanovanja = nova_adresa, telefon = novi_telefon, jmbg = novi_jmbg, datum_rodjenja = novi_datum_rodjenja
 WHERE broj_indeksa = novi_br_indeksa;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ts_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ts_create`(IN nova_ustanova VARCHAR(32), IN novi_studijski_program VARCHAR(32), IN novi_studenti_iz_ustanove VARCHAR(32), IN nova_struka VARCHAR(32), IN nova_skolska_godina VARCHAR(32), IN novi_br_indeksa VARCHAR(32), IN nova_godina_studija VARCHAR(32), IN novi_blok VARCHAR(32), IN novi_redni_broj_upisa VARCHAR(32), IN novi_datum_upisa VARCHAR(32), IN novi_datum_overe VARCHAR(32), IN novi_espb_pocetni VARCHAR(32), IN novi_espb_krajnji VARCHAR(32))
BEGIN
INSERT INTO tok_studija(ustanova, studijski_program, studenti_iz_ustanove, struka, skolska_godina, studenti_broj_indeksa, godina_studija, blok, redni_broj_upisa, datum_upisa, datum_overe, espb_pocetni, espb_krajnji) values(nova_ustanova, novi_studijski_program, novi_studenti_iz_ustanove, nova_struka, nova_skolska_godina, novi_br_indeksa, nova_godina_studija, novi_blok, novi_redni_broj_upisa, novi_datum_upisa, novi_datum_overe, novi_espb_pocetni, novi_espb_krajnji);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ts_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ts_delete`(IN id VARCHAR(32))
BEGIN
	DELETE FROM tok_studija
    WHERE studenti_broj_indeksa = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ts_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ts_read`()
BEGIN
	SELECT * FROM tok_studija;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ts_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ts_search`(IN trazi_ustanovu VARCHAR(32), IN trazi_studijski_program VARCHAR(32), IN trazi_studente_iz_ustanove VARCHAR(32), IN trazi_struku VARCHAR(32), IN trazi_skolsku_godinu VARCHAR(32), IN trazi_studenti_indeks VARCHAR(32), IN trazi_godinu_studija VARCHAR(32), IN trazi_blok VARCHAR(32), IN trazi_redni_broj VARCHAR(32), IN trazi_datum_upisa VARCHAR(32), IN trazi_datum_overe VARCHAR(32), IN trazi_espb_pocetni VARCHAR(32), IN trazi_espb_krajnji VARCHAR(32))
BEGIN
	SELECT * FROM tok_studija
    WHERE ustanova = trazi_ustanovu OR studijski_program = trazi_studijski_program OR studenti_iz_ustanove = trazi_studente_iz_ustanove OR struka = trazi_struku OR skolska_godina = trazi_skolsku_godinu OR studenti_broj_indeksa = trazi_studenti_indeks OR godina_studija = trazi_godinu_studija OR blok = trazi_blok OR redni_broj_upisa = trazi_redni_broj OR datum_upisa = trazi_datum_upisa OR datum_overe = trazi_datum_overe OR espb_pocetni = trazi_espb_pocetni OR espb_krajnji = trazi_espb_krajnji;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ts_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ts_update`(IN nova_ustanova VARCHAR(32), IN novi_studijski_program VARCHAR(32), IN novi_studenti_iz_ustanove VARCHAR(32), IN nova_struka VARCHAR(32), IN nova_skolska_godina VARCHAR(32), IN novi_br_indeksa VARCHAR(32), IN nova_godina_studija VARCHAR(32), IN novi_blok VARCHAR(32), IN novi_redni_broj_upisa VARCHAR(32), IN novi_datum_upisa VARCHAR(32), IN novi_datum_overe VARCHAR(32), IN novi_espb_pocetni VARCHAR(32), IN novi_espb_krajnji VARCHAR(32))
BEGIN
 UPDATE tok_studija
 SET ustanova = nova_ustanova, studijski_program = novi_studijski_program, studenti_iz_ustanove = novi_studenti_iz_ustanove, struka = nova_struka, skolska_godina = nova_skolska_godina, godina_studija = nova_godina_studija, blok = novi_blok, redni_broj_upisa = novi_redni_broj_upisa, datum_upisa = novi_datum_upisa, datum_overe = novi_datum_overe, espb_pocetni = novi_espb_pocetni, espb_krajnji = novi_espb_krajnji
 WHERE studenti_broj_indeksa = novi_br_indeksa;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vu_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vu_create`(IN nova_oznaka VARCHAR(32), IN novi_naziv VARCHAR(32), IN nova_adresa VARCHAR(32))
BEGIN
INSERT INTO visokoskolska_ustanova(oznaka, naziv, adresa) values(nova_oznaka, novi_naziv, nova_adresa);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vu_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vu_delete`(IN id VARCHAR(32))
BEGIN
	DELETE FROM visokoskolska_ustanova
    WHERE oznaka = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vu_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vu_read`()
BEGIN
	SELECT * FROM visokoskolska_ustanova;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vu_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vu_search`(IN trazi_oznaku VARCHAR(32), IN trazi_naziv VARCHAR(32), IN trazi_adresu VARCHAR(32))
BEGIN
	SELECT * FROM visokoskolska_ustanova
    WHERE oznaka = trazi_oznaku OR naziv = trazi_naziv OR adresa = trazi_adresu;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vu_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vu_update`(IN nova_oznaka VARCHAR(32), IN novi_naziv VARCHAR(32), IN nova_adresa VARCHAR(32))
BEGIN
 UPDATE visokoskolska_ustanova
 SET naziv = novi_naziv, adresa = nova_adresa
 WHERE oznaka = nova_oznaka;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-30  6:55:51
